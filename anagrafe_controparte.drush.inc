<?php

	/**
	 * @file Drush User Management commands
	 */
	function anagrafe_controparte_drush_help($section) {
	  switch ($section) {
	    case 'meta:anagrafe_controparte:title':
	      return dt('Anagrafica Controparte commands');
	  }
	}


	/**
	 * Implementation of hook_drush_command().
	 */
	function anagrafe_controparte_drush_command() {
	
		$items['anagrafe_controparte_update'] = array(
			'callback' => 'drush_anagrafe_controparte_update',
			'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
			'description' => 'Update Anagrafe Controparte fields from old format to new one.',
			'aliases' => array('acup'),
			'examples' => array(
				'drush anagrafe-controparte-update' => 
				'Update Anagrafe Controparte fields from old format to new one.',
			),
		);

		return $items;
	}

	/**
	 * Update Anagrafe Controparte fields from old format to new one.
	 */

	function drush_anagrafe_controparte_update(){
		// Load all nodes of type 'anagrafe_controparte'
		$query = "SELECT COUNT(DISTINCT nid) FROM {node} WHERE type LIKE '%s' AND status=%d";
		$count = db_result(db_query($query, 'anagrafe_controparte', 1));
		
		$query = "SELECT nid, title FROM {node} WHERE type LIKE '%s' AND status=%d";
		$result = db_query($query, 'anagrafe_controparte', 1);
		
		while ($db_node = drush_db_fetch_object($result)) {
			//drush_print(print_r($db_node, true));	
			drush_log(
				dt(
					'Operation on Anagrafe Controparte !title (!nid).', 
					array('!title' => $db_node->title, '!nid' => $db_node->nid)
				),
				'ok'
			);
			_anagrafe_controparte_node_opeation($db_node->nid);
			drush_print();
		}
		
		drush_print(dt('Star all batch process:'));
		drush_backend_batch_process(); // Batch Processors Start Here
		
		drush_log(dt('Anagrafe Controparte converted.'), 'ok');
		return;
	}

	// Operation on node
	function _anagrafe_controparte_node_opeation($nid){
		// Load all "Anagrafe Controparte" tid
		$node = node_load($nid);
		if(!isset($node->field_controparte_importazione)) return;
		$title = $node->title;
		$tid_import = $node->field_controparte_importazione;
		unset($node);
		$term_list = array();
		$_term = array();
		foreach($tid_import as $k => $tid){
			$tid_import[$k] = $tid['value'];
			$_term = taxonomy_get_term($tid['value']);
			$term_list[] = $_term->name; 
		}
		unset($tid);
		unset($k);
		$tid_import = implode($tid_import, ', ');

		// Load all Scheda Rilevazione with tid
		$query = "SELECT COUNT(DISTINCT node.nid)
			FROM {node} node 
			INNER JOIN {term_node} term_node ON node.vid = term_node.vid
			WHERE (node.type IN ('%s')) AND (term_node.tid IN (%s));";
		$count = db_query($query, 'scheda_problemi', $tid_import);
		$count = db_result($count);
		
		drush_log(
			dt(
				'Found !count Schede Problema with taxonomy terms !terms', 
				array('!count' => $count, '!terms' => implode($term_list, ', '))
			),
			'ok'
		);
		
		
		// Set and configure the batch
		$batch = array(
			'operations' => array(
				array('_batch_anagrafe_controparte_main', array($count, $nid, $tid_import, $title)),
			),
		);
		$batch['progressive'] = FALSE;
  		batch_set($batch);
  		// Batch procedure start in drush_anagrafe_controparte_update function 
  		drush_log(
			dt(
				'Set batch procedure for !count Schede Problema.', 
				array('!count' => $count)
			),
			'ok'
		);
 	 	
		return;
	}
	
	
	/**
	 * Batch processor esecution
	*/
	function _batch_anagrafe_controparte_main($count, $nid, $tid_import, $title, &$context){
		if (!isset($context['sandbox']['progress'])) {
		 $context['sandbox']['progress'] = 0;
		 $context['sandbox']['max'] = $count;
		 //drush_print_r($tid_import);
		}
		
		$query = "SELECT node.nid AS nid
			FROM {node} node 
			INNER JOIN {term_node} term_node ON node.vid = term_node.vid
			WHERE (node.type IN ('%s')) AND (term_node.tid IN (%s))
			GROUP BY nid
			LIMIT %d, %d
			;";
		$results = db_query($query, 'scheda_problemi', $tid_import, $context['sandbox']['progress'], 1);
		
		while ($result = drush_db_fetch_object($results)) {
			$controparte = array();
			$scheda_problema = node_load($result->nid);
			// Save old reference
			/*
			foreach($scheda_problema->field_scheda_problema_contropart as $reference){
				$controparte[$reference['nid']] = $reference['nid'];
			}
			unset($reference);
			*/
			$controparte[$nid] = $nid;
			$scheda_problema->field_scheda_problema_contropart = array();
			
			foreach($controparte as $reference){
				$scheda_problema->field_scheda_problema_contropart[]['nid'] = $reference;
			}
			unset($reference);
			@node_save($scheda_problema);
			$_percent = (100*($context['sandbox']['progress']+1))/$count;
			$_percent = number_format($_percent, 2);
			drush_log(
				dt(
					"!percent% Updated Scheda Rilevazione !title \t (nid: !nid) \t (!controparte)", 
					array('!percent' => $_percent, '!title' => $scheda_problema->title, '!nid' => $scheda_problema->nid, '!controparte' => $title)
				),
				'ok'
			);
			unset($controparte);
			unset($scheda_problema);
		}
		
		$context['sandbox']['progress']++;
		if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
		 $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
		}
	}
	
	
