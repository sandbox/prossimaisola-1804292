<?php

/**
 * Implementation of hook_conditional_fields_default_fields().
 */
function anagrafe_controparte_conditional_fields_default_fields() {
$items = array();
$items[] = array (
  'control_field_name' => 'field_controparte_tipo_societa',
  'field_name' => 'field_controparte_iva_cf',
  'type' => 'anagrafe_controparte',
  'trigger_values' => 
  array (
    'normal' => 'normal',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_b_modalit_contrattuali',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Beni di consumo' => 'Beni di consumo',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_modalit_contrattuali',
  'field_name' => 'field_b_contratti_fuori_locali_',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti conclusi fuori dai locali commerciali' => 'Contratti conclusi fuori dai locali commerciali',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_modalit_contrattuali',
  'field_name' => 'field_b_contratti_a_distanza',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti a distanza' => 'Contratti a distanza',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_contratti_fuori_locali_',
  'field_name' => 'field_b_altro_contratti_conclus',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_contratti_a_distanza',
  'field_name' => 'field_b_altro_contratti_distanz',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_modalit_contrattuali',
  'field_name' => 'field_b_altro_modalit_contrattu',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_b_scelta_del_bene_determi',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Beni di consumo' => 'Beni di consumo',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_modalit_contrattuali',
  'field_name' => 'field_b_altro_scelta_del_bene',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_b_modalit_pagamento',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Beni di consumo' => 'Beni di consumo',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_modalit_pagamento',
  'field_name' => 'field_b_altro_modalit_di_pagame',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_b_settore',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Beni di consumo' => 'Beni di consumo',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_settore',
  'field_name' => 'field_b_altro_settore',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_b_oggetto_della_controver',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Beni di consumo' => 'Beni di consumo',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_s_modalit_contrattuali',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Servizi' => 'Servizi',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_modalit_contrattuali',
  'field_name' => 'field_s_contratti_conclusi_fuor',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti conclusi fuori dai locali commerciali' => 'Contratti conclusi fuori dai locali commerciali',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_modalit_contrattuali',
  'field_name' => 'field_s_contratti_a_distanza',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti a distanza' => 'Contratti a distanza',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_contratti_conclusi_fuor',
  'field_name' => 'field_s_altro_contratti_conclus',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_contratti_a_distanza',
  'field_name' => 'field_s_altro_contratti_distanz',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_modalit_contrattuali',
  'field_name' => 'field_s_altro_modalit_contrattu',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_s_scelta_del_servizio',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Servizi' => 'Servizi',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_scelta_del_servizio',
  'field_name' => 'field_s_altro_scelta_del_serviz',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_s_modalit_di_pagamento',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Servizi' => 'Servizi',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_modalit_di_pagamento',
  'field_name' => 'field_s_altro_modalit_di_pagame',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_s_settore',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Servizi' => 'Servizi',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_assicurazioni',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Assicurazioni' => 'Assicurazioni',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_credito_finanza',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Credito e Finanza' => 'Credito e Finanza',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_energia',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Energia' => 'Energia',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_istruzione',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Istruzione' => 'Istruzione',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_giustizia',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Giustizia' => 'Giustizia',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_pubblica_ammini',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Pubblica Amministrazione' => 'Pubblica Amministrazione',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_ristorazione_e_',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Ristorazione e strutture ricettive' => 'Ristorazione e strutture ricettive',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_sanit',
  'type' => 'scheda_problemi',
  'trigger_values' => false,
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_telecomunicazio',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Telecomunicazioni' => 'Telecomunicazioni',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore_telecomunicazio',
  'field_name' => 'field_s_settore_telec_tvdigital',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Tv digitale' => 'Tv digitale',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_trasporti',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Trasporti' => 'Trasporti',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_servizi_turisti',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Servizi turistici' => 'Servizi turistici',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_settore',
  'field_name' => 'field_s_settore_viabilit_e_perc',
  'type' => 'scheda_problemi',
  'trigger_values' => false,
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_s_oggetto_della_controver',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Servizi' => 'Servizi',
  ),
);
$items[] = array (
  'control_field_name' => 'field_s_oggetto_della_controver',
  'field_name' => 'field_s_altro_oggetto_controver',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_modalit_contrattuali',
  'field_name' => 'field_i_contratti_di_locazione',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti di locazione' => 'Contratti di locazione',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_contratti_di_locazione',
  'field_name' => 'field_i_altro_contratti_di_loca',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_modalit_contrattuali',
  'field_name' => 'field_i_altro_modalit_contrattu',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_i_modalit_di_pagamento',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Immobiliare/Locazione/Multiproprietà' => 'Immobiliare/Locazione/Multiproprietà',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_settore',
  'field_name' => 'field_i_locazioni_ad_uso_abitat',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Locazioni ad uso abitativo, accessori e pertinenze' => 'Locazioni ad uso abitativo, accessori e pertinenze',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_settore',
  'field_name' => 'field_i_altro_settore',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_delibere_di_condominio',
  'field_name' => 'field_i_altro_oggetto_della_con',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_i_scelta_del_bene_immobil',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Immobiliare/Locazione/Multiproprietà' => 'Immobiliare/Locazione/Multiproprietà',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_i_modalit_contrattuali',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Immobiliare/Locazione/Multiproprietà' => 'Immobiliare/Locazione/Multiproprietà',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_modalit_contrattuali',
  'field_name' => 'field_i_contratti_conclusi_fuor',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti conclusi fuori dai locali commerciali' => 'Contratti conclusi fuori dai locali commerciali',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_i_settore',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Immobiliare/Locazione/Multiproprietà' => 'Immobiliare/Locazione/Multiproprietà',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_oggetto_della_controver',
  'field_name' => 'field_i_sfratto',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Sfratto' => 'Sfratto',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_sfratto',
  'field_name' => 'field_i_altro_sfratto',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_oggetto_segnalazione',
  'field_name' => 'field_i_oggetto_della_controver',
  'type' => 'scheda_problemi',
  'trigger_values' => false,
);
$items[] = array (
  'control_field_name' => 'field_i_scelta_del_bene_immobil',
  'field_name' => 'field_i_altro_scelta_del_bene',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_modalit_contrattuali',
  'field_name' => 'field_i_contratti_a_distanza',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Contratti a distanza' => 'Contratti a distanza',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_contratti_a_distanza',
  'field_name' => 'field_i_altro_contratti_a_dista',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_oggetto_della_controver',
  'field_name' => 'field_i_delibere_di_condominio',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Delibere di condominio/comunione' => 'Delibere di condominio/comunione',
  ),
);
$items[] = array (
  'control_field_name' => 'field_stato_della_pratica_0',
  'field_name' => 'field_chiusa_stato_della_pratic',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Chiusa' => 'Chiusa',
  ),
);
$items[] = array (
  'control_field_name' => 'field_chiusa_stato_della_pratic',
  'field_name' => 'field_altro_chiusa',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_stato_della_pratica_0',
  'field_name' => 'field_altro_stato_della_pratica',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_b_scelta_del_bene_determi',
  'field_name' => 'field_b_altro_scelta_del_bene',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'Altro' => 'Altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_contratti_conclusi_fuor',
  'field_name' => 'field_i_altro_contratti_conclus',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
$items[] = array (
  'control_field_name' => 'field_i_delibere_di_condominio',
  'field_name' => 'field_i_altro_delibere_condomin',
  'type' => 'scheda_problemi',
  'trigger_values' => 
  array (
    'altro' => 'altro',
  ),
);
return $items;
}
