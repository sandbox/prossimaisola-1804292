<?php

/**
 * Implementation of hook_content_default_fields().
 */
function anagrafe_controparte_content_default_fields() {
  $fields = array();

  // Exported field: field_controparte_altri_sede
  $fields['anagrafe_controparte-field_controparte_altri_sede'] = array(
    'field_name' => 'field_controparte_altri_sede',
    'type_name' => 'anagrafe_controparte',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'location',
    'required' => '0',
    'multiple' => '1',
    'module' => 'location_cck',
    'active' => '1',
    'location_settings' => array(
      'form' => array(
        'fields' => array(
          'name' => array(
            'collect' => '1',
            'default' => '',
            'weight' => '2',
          ),
          'street' => array(
            'collect' => '1',
            'default' => '',
            'weight' => '4',
          ),
          'additional' => array(
            'collect' => '1',
            'default' => '',
            'weight' => '6',
          ),
          'city' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '8',
          ),
          'province' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '10',
          ),
          'postal_code' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '12',
          ),
          'country' => array(
            'collect' => '1',
            'default' => 'it',
            'weight' => '14',
          ),
          'locpick' => array(
            'collect' => '1',
            'weight' => '20',
          ),
          'phone' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '25',
          ),
          'fax' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '30',
          ),
        ),
      ),
      'display' => array(
        'hide' => array(
          'name' => 0,
          'street' => 0,
          'additional' => 0,
          'city' => 0,
          'province' => 0,
          'postal_code' => 0,
          'country' => 0,
          'locpick' => 0,
          'fax' => 0,
          'phone' => 0,
          'province_name' => 0,
          'country_name' => 0,
          'map_link' => 0,
          'coords' => 0,
        ),
      ),
    ),
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'name' => '',
          'street' => '',
          'additional' => '',
          'country' => 'it',
          'locpick' => array(
            'user_latitude' => '',
            'user_longitude' => '',
          ),
          'cck_preview_in_progress' => TRUE,
          'location_settings' => array(
            'form' => array(
              'fields' => array(
                'lid' => array(
                  'default' => FALSE,
                ),
                'name' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => 2,
                ),
                'street' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => 4,
                ),
                'additional' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => 6,
                ),
                'city' => array(
                  'default' => '',
                  'collect' => 0,
                  'weight' => 8,
                ),
                'province' => array(
                  'default' => '',
                  'collect' => 0,
                  'weight' => 10,
                ),
                'postal_code' => array(
                  'default' => '',
                  'collect' => 0,
                  'weight' => 12,
                ),
                'country' => array(
                  'default' => 'it',
                  'collect' => 1,
                  'weight' => 14,
                ),
                'locpick' => array(
                  'default' => FALSE,
                  'collect' => 1,
                  'weight' => 20,
                  'nodiff' => TRUE,
                ),
                'latitude' => array(
                  'default' => 0,
                ),
                'longitude' => array(
                  'default' => 0,
                ),
                'source' => array(
                  'default' => 0,
                ),
                'is_primary' => array(
                  'default' => 0,
                ),
                'delete_location' => array(
                  'default' => FALSE,
                  'nodiff' => TRUE,
                ),
                'fax' => array(
                  'default' => '',
                  'collect' => 0,
                  'weight' => 30,
                ),
                'phone' => array(
                  'default' => '',
                  'collect' => 0,
                  'weight' => 25,
                ),
              ),
            ),
          ),
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Indirizo altre sedi',
      'weight' => 0,
      'description' => '',
      'type' => 'location',
      'module' => 'location_cck',
    ),
  );

  // Exported field: field_controparte_importazione
  $fields['anagrafe_controparte-field_controparte_importazione'] = array(
    'field_name' => 'field_controparte_importazione',
    'type_name' => 'anagrafe_controparte',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'content_taxonomy',
    'required' => '0',
    'multiple' => '1',
    'module' => 'content_taxonomy',
    'active' => '1',
    'save_term_node' => 0,
    'vid' => '31',
    'parent' => '0',
    'parent_php_code' => '',
    'depth' => '',
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'new_terms' => 'deny',
      'extra_parent' => '0',
      'maxlength' => '255',
      'active_tags' => NULL,
      'default_value' => array(
        '0' => array(
          'value' => NULL,
        ),
      ),
      'default_value_php' => NULL,
      'group_parent' => '0',
      'show_depth' => 1,
      'label' => 'Importazione',
      'weight' => '1',
      'description' => '',
      'type' => 'content_taxonomy_select',
      'module' => 'content_taxonomy_options',
    ),
  );

  // Exported field: field_controparte_iva_cf
  $fields['anagrafe_controparte-field_controparte_iva_cf'] = array(
    'field_name' => 'field_controparte_iva_cf',
    'type_name' => 'anagrafe_controparte',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '16',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'rows' => 5,
      'size' => '30',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_controparte_iva_cf][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Partita IVA/Codice Fiscale',
      'weight' => '-2',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_controparte_ragione_social
  $fields['anagrafe_controparte-field_controparte_ragione_social'] = array(
    'field_name' => 'field_controparte_ragione_social',
    'type_name' => 'anagrafe_controparte',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'rows' => 5,
      'size' => '30',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_controparte_ragione_social][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Ragione Sociale',
      'weight' => '-4',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_controparte_sede_legale
  $fields['anagrafe_controparte-field_controparte_sede_legale'] = array(
    'field_name' => 'field_controparte_sede_legale',
    'type_name' => 'anagrafe_controparte',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'location',
    'required' => '1',
    'multiple' => '0',
    'module' => 'location_cck',
    'active' => '1',
    'location_settings' => array(
      'form' => array(
        'fields' => array(
          'street' => array(
            'collect' => '2',
            'default' => '',
            'weight' => '-100',
          ),
          'name' => array(
            'collect' => '1',
            'default' => '',
            'weight' => '-99',
          ),
          'additional' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '-98',
          ),
          'city' => array(
            'collect' => '2',
            'default' => '',
            'weight' => '-97',
          ),
          'province' => array(
            'collect' => '2',
            'default' => '',
            'weight' => '-96',
          ),
          'postal_code' => array(
            'collect' => '2',
            'default' => '',
            'weight' => '-95',
          ),
          'country' => array(
            'collect' => '1',
            'default' => 'it',
            'weight' => '-94',
          ),
          'locpick' => array(
            'collect' => '0',
            'weight' => '-93',
          ),
          'phone' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '-92',
          ),
          'fax' => array(
            'collect' => '0',
            'default' => '',
            'weight' => '-91',
          ),
        ),
      ),
      'display' => array(
        'hide' => array(
          'name' => 0,
          'street' => 0,
          'additional' => 0,
          'city' => 0,
          'province' => 0,
          'postal_code' => 0,
          'country' => 0,
          'locpick' => 0,
          'fax' => 0,
          'phone' => 0,
          'province_name' => 0,
          'country_name' => 0,
          'map_link' => 0,
          'coords' => 0,
        ),
      ),
    ),
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'street' => '',
          'name' => '',
          'additional' => '',
          'city' => '',
          'province' => '',
          'postal_code' => '',
          'country' => 'it',
          'cck_preview_in_progress' => TRUE,
          'location_settings' => array(
            'form' => array(
              'fields' => array(
                'street' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => '-100',
                ),
                'name' => array(
                  'default' => '',
                  'collect' => '1',
                  'weight' => '-99',
                ),
                'additional' => array(
                  'default' => '',
                  'collect' => '0',
                  'weight' => '-98',
                ),
                'city' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => '-97',
                ),
                'province' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => '-96',
                ),
                'postal_code' => array(
                  'default' => '',
                  'collect' => 1,
                  'weight' => '-95',
                ),
                'country' => array(
                  'default' => 'it',
                  'collect' => '1',
                  'weight' => '-94',
                ),
                'locpick' => array(
                  'default' => array(
                    'user_latitude' => '',
                    'user_longitude' => '',
                  ),
                  'collect' => '0',
                  'weight' => '-93',
                  'nodiff' => TRUE,
                ),
                'phone' => array(
                  'default' => '',
                  'collect' => '0',
                  'weight' => '-92',
                ),
                'fax' => array(
                  'default' => '',
                  'collect' => '0',
                  'weight' => '-91',
                ),
                'cck_preview_in_progress' => array(
                  'default' => TRUE,
                ),
                'location_settings' => array(
                  'default' => array(
                    'form' => array(
                      'fields' => array(
                        'street' => array(
                          'default' => '',
                          'collect' => 1,
                          'weight' => '-100',
                        ),
                        'name' => array(
                          'default' => '',
                          'collect' => '1',
                          'weight' => '-99',
                        ),
                        'additional' => array(
                          'default' => '',
                          'collect' => '0',
                          'weight' => '-98',
                        ),
                        'city' => array(
                          'default' => '',
                          'collect' => 1,
                          'weight' => '-97',
                        ),
                        'province' => array(
                          'default' => '',
                          'collect' => 1,
                          'weight' => '-96',
                        ),
                        'postal_code' => array(
                          'default' => '',
                          'collect' => 1,
                          'weight' => '-95',
                        ),
                        'country' => array(
                          'default' => 'it',
                          'collect' => '1',
                          'weight' => '-94',
                        ),
                        'locpick' => array(
                          'default' => array(
                            'user_latitude' => '',
                            'user_longitude' => '',
                          ),
                          'collect' => '1',
                          'weight' => '-93',
                          'nodiff' => TRUE,
                        ),
                        'phone' => array(
                          'default' => '',
                          'collect' => '0',
                          'weight' => '-92',
                        ),
                        'fax' => array(
                          'default' => '',
                          'collect' => '0',
                          'weight' => '-91',
                        ),
                        'cck_preview_in_progress' => array(
                          'default' => TRUE,
                        ),
                        'location_settings' => array(
                          'default' => array(
                            'form' => array(
                              'fields' => array(
                                'name' => array(
                                  'default' => '',
                                  'collect' => '1',
                                  'weight' => '2',
                                ),
                                'street' => array(
                                  'default' => '',
                                  'collect' => '1',
                                  'weight' => '4',
                                ),
                                'additional' => array(
                                  'default' => '',
                                  'collect' => '1',
                                  'weight' => '6',
                                ),
                                'city' => array(
                                  'default' => '',
                                  'collect' => '0',
                                  'weight' => '8',
                                ),
                                'province' => array(
                                  'default' => '',
                                  'collect' => '0',
                                  'weight' => '10',
                                ),
                                'postal_code' => array(
                                  'default' => '',
                                  'collect' => '0',
                                  'weight' => '12',
                                ),
                                'country' => array(
                                  'default' => 'it',
                                  'collect' => '1',
                                  'weight' => '14',
                                ),
                                'locpick' => array(
                                  'default' => array(
                                    'user_latitude' => '',
                                    'user_longitude' => '',
                                  ),
                                  'collect' => '1',
                                  'weight' => '20',
                                  'nodiff' => TRUE,
                                ),
                                'phone' => array(
                                  'default' => '',
                                  'collect' => '0',
                                  'weight' => '25',
                                ),
                                'fax' => array(
                                  'default' => '',
                                  'collect' => '0',
                                  'weight' => '30',
                                ),
                                'cck_preview_in_progress' => array(
                                  'default' => TRUE,
                                ),
                                'location_settings' => array(
                                  'default' => array(
                                    'form' => array(
                                      'fields' => array(
                                        'lid' => array(
                                          'default' => FALSE,
                                        ),
                                        'name' => array(
                                          'default' => '',
                                          'collect' => 1,
                                          'weight' => 2,
                                        ),
                                        'street' => array(
                                          'default' => '',
                                          'collect' => 1,
                                          'weight' => 4,
                                        ),
                                        'additional' => array(
                                          'default' => '',
                                          'collect' => 1,
                                          'weight' => 6,
                                        ),
                                        'city' => array(
                                          'default' => '',
                                          'collect' => 0,
                                          'weight' => 8,
                                        ),
                                        'province' => array(
                                          'default' => '',
                                          'collect' => 0,
                                          'weight' => 10,
                                        ),
                                        'postal_code' => array(
                                          'default' => '',
                                          'collect' => 0,
                                          'weight' => 12,
                                        ),
                                        'country' => array(
                                          'default' => 'it',
                                          'collect' => 1,
                                          'weight' => 14,
                                        ),
                                        'locpick' => array(
                                          'default' => FALSE,
                                          'collect' => 1,
                                          'weight' => 20,
                                          'nodiff' => TRUE,
                                        ),
                                        'latitude' => array(
                                          'default' => 0,
                                        ),
                                        'longitude' => array(
                                          'default' => 0,
                                        ),
                                        'source' => array(
                                          'default' => 0,
                                        ),
                                        'is_primary' => array(
                                          'default' => 0,
                                        ),
                                        'delete_location' => array(
                                          'default' => FALSE,
                                          'nodiff' => TRUE,
                                        ),
                                        'fax' => array(
                                          'default' => '',
                                          'collect' => 0,
                                          'weight' => 30,
                                        ),
                                        'phone' => array(
                                          'default' => '',
                                          'collect' => 0,
                                          'weight' => 25,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                'lid' => array(
                                  'default' => FALSE,
                                ),
                                'latitude' => array(
                                  'default' => 0,
                                ),
                                'longitude' => array(
                                  'default' => 0,
                                ),
                                'source' => array(
                                  'default' => 0,
                                ),
                                'is_primary' => array(
                                  'default' => 0,
                                ),
                                'delete_location' => array(
                                  'default' => FALSE,
                                  'nodiff' => TRUE,
                                ),
                              ),
                            ),
                            'display' => array(
                              'hide' => array(
                                'name' => 'name',
                                'city' => 'city',
                                'province' => 'province',
                                'postal_code' => 'postal_code',
                                'fax' => 'fax',
                                'phone' => 'phone',
                                'street' => 0,
                                'additional' => 0,
                                'country' => 0,
                                'locpick' => 0,
                                'province_name' => 0,
                                'country_name' => 0,
                                'map_link' => 0,
                                'coords' => 0,
                              ),
                            ),
                          ),
                        ),
                        'lid' => array(
                          'default' => FALSE,
                        ),
                        'latitude' => array(
                          'default' => 0,
                        ),
                        'longitude' => array(
                          'default' => 0,
                        ),
                        'source' => array(
                          'default' => 0,
                        ),
                        'is_primary' => array(
                          'default' => 0,
                        ),
                        'delete_location' => array(
                          'default' => FALSE,
                          'nodiff' => TRUE,
                        ),
                      ),
                    ),
                    'display' => array(
                      'hide' => array(
                        'name' => 0,
                        'street' => 0,
                        'additional' => 0,
                        'city' => 0,
                        'province' => 0,
                        'postal_code' => 0,
                        'country' => 0,
                        'locpick' => 0,
                        'fax' => 0,
                        'phone' => 0,
                        'province_name' => 0,
                        'country_name' => 0,
                        'map_link' => 0,
                        'coords' => 0,
                      ),
                    ),
                  ),
                ),
                'lid' => array(
                  'default' => FALSE,
                ),
                'latitude' => array(
                  'default' => 0,
                ),
                'longitude' => array(
                  'default' => 0,
                ),
                'source' => array(
                  'default' => 0,
                ),
                'is_primary' => array(
                  'default' => 0,
                ),
                'delete_location' => array(
                  'default' => FALSE,
                  'nodiff' => TRUE,
                ),
              ),
            ),
            'display' => array(
              'hide' => array(
                'name' => 0,
                'street' => 0,
                'additional' => 0,
                'city' => 0,
                'province' => 0,
                'postal_code' => 0,
                'country' => 0,
                'locpick' => 0,
                'fax' => 0,
                'phone' => 0,
                'province_name' => 0,
                'country_name' => 0,
                'map_link' => 0,
                'coords' => 0,
              ),
            ),
          ),
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Indirizzo sede legale',
      'weight' => '-1',
      'description' => '',
      'type' => 'location',
      'module' => 'location_cck',
    ),
  );

  // Exported field: field_controparte_tipo_societa
  $fields['anagrafe_controparte-field_controparte_tipo_societa'] = array(
    'field_name' => 'field_controparte_tipo_societa',
    'type_name' => 'anagrafe_controparte',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'normal|Società normale
ghost|Società fantasma
',
    'allowed_values_php' => '',
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => 'normal',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Tipo società',
      'weight' => '-3',
      'description' => '',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_scheda_problema_contropart
  $fields['scheda_problemi-field_scheda_problema_contropart'] = array(
    'field_name' => 'field_scheda_problema_contropart',
    'type_name' => 'scheda_problemi',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_plain' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'email_html' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'anagrafe_controparte' => 'anagrafe_controparte',
      'argomenti_immagini' => 0,
      'articolo' => 0,
      'associazione' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'comment_test' => 0,
      'contact' => 0,
      'domanda' => 0,
      'faq' => 0,
      'event' => 0,
      'feed' => 0,
      'feed_item' => 0,
      'group' => 0,
      'home_categoria' => 0,
      'image' => 0,
      'fattispecie_particolare' => 0,
      'landingpage' => 0,
      'blog' => 0,
      'simplenews' => 0,
      'newsfeed' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'richiesta' => 0,
      'risposta_associazione' => 0,
      'scheda_associazione' => 0,
      'scheda_approfondimento' => 0,
      'scheda_didattica' => 0,
      'scheda_problemi' => 0,
      'home_statica' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'field_permissions' => array(
      'create' => 0,
      'edit' => 0,
      'edit own' => 0,
      'view' => 0,
      'view own' => 0,
    ),
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'multistep' => NULL,
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Controparte',
      'weight' => '27',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Controparte');
  t('Importazione');
  t('Indirizo altre sedi');
  t('Indirizzo sede legale');
  t('Partita IVA/Codice Fiscale');
  t('Ragione Sociale');
  t('Tipo società');

  return $fields;
}
