<?php

/**
 * Implementation of hook_strongarm().
 */
function anagrafe_controparte_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_anagrafe_controparte';
  $strongarm->value = '0';

  $export['ant_anagrafe_controparte'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_anagrafe_controparte';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '6',
    'author' => '7',
    'options' => '8',
    'comment_settings' => '11',
    'menu' => '2',
    'book' => '5',
    'path' => '10',
    'attachments' => '3',
    'print' => '9',
    'workflow' => '4',
  );

  $export['content_extra_weights_anagrafe_controparte'] = $strongarm;
  return $export;
}
