<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function anagrafe_controparte_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function anagrafe_controparte_node_info() {
  $items = array(
    'anagrafe_controparte' => array(
      'name' => t('Anagrafe Controparte'),
      'module' => 'features',
      'description' => t('Scheda dedicata all\'azienda o ente a cui si riferiscono le segnalazioni del consumatore.'),
      'has_title' => '1',
      'title_label' => t('Nome azienda/ente'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
